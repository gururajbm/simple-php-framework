<?php
namespace App\Model;
Use Eloquent;

class User extends Eloquent {
	protected $tabel = 'users';
	protected $primaryKey = 'id';
	protected $guarded = array();

	public static function getUser() {
		return User::all();
	}
}
?>