<?php

require_once __Dir__ . '/../../vendor/autoload.php';

use App\Config\Database as Conn;
use Illuminate\Container\Container;
use Illuminate\Database\Capsule;
use Illuminate\Events\Dispatcher;

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../../');
$dotenv->overload();

$route = new Pux\Mux;

$capsule = new Illuminate\Database\Capsule\Manager;
$capsule->addConnection(Conn::mysqlConnection());
$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->bootEloquent();
$capsule->setAsGlobal();

if (getenv("ENV") == 'Development' || getenv("ENV") == 'Staging') {
	ini_set("display_errors", 1);
	error_reporting(E_ALL);
} else {
	ini_set("display_errors", 0);
	error_reporting(0);
}

set_exception_handler(function ($exception) {

	view('error.500', array('data' => $exception), 500, 'Internal Server Error');
});