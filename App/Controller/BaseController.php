<?php
namespace App\Controller;
use Pux\Controller as InternalController;
use Request;

class BaseController extends InternalController {

	public $request;

	public function __construct() {
		$this->request = Request::createFromGlobals();
	}

}
