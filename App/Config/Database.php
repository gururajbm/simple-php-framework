<?php
namespace App\Config;

class Database {

	public static function mysqlConnection() {

		return array(
			'driver' => 'mysql',
			'database' => getenv('DATABASE_NAME'),
			'username' => getenv('DATABASE_USERNAME'),
			'password' => getenv('DATABASE_PASSWORD'),
			'host' => getenv('DATABASE_HOST'),
			'collation' => 'utf8_bin',

		);
	}
}

?>