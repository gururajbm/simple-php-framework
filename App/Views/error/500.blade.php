<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Tangerine">
	<style type="text/css">
		body{
			background-color: grey;
		}

		#message {
			padding: 1%;
			background-color:#e4e4e4;
			font-size: 2em;
			font-family: 'open sans','Roboto',serif;
			color:red;
			border-bottom: 5px;

		}
		#stacktrace{
			padding: 1%;

			font-size:1 em;
			font-family: 'open sans','Roboto',serif;
			color:white;
			border-bottom: 5px;
		}
		.mar-top-10 {margin-top:10x;}

		.mar-bottom-10{
			margin-bottom: 10px;s
		}




	</style>

</head>
<body>
<div class="divMessage">
	<p id="message">{{ $data->getMessage() }} in {{$data->getFile()}} at {{$data->getLine()}}</p>
</div>
<div class="mar-top-10 mar-bottom-10"></div>
<div id="stacktrace">
	{{$data->getTraceAsString()}}
</div>
</body>
</html>