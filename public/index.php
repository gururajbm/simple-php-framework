<?php
require_once __Dir__ . '/../App/bootstrap/app.php';
require '../App/routes.php';
use Pux\Executor;

$routeDispatcher = $route->dispatch('/');
if (isset($_SERVER['PATH_INFO'])) {
	$routeDispatcher = $route->dispatch($_SERVER['PATH_INFO']);

}
if (!empty($routeDispatcher) && $routeDispatcher) {
	Executor::execute($routeDispatcher);
} else {
	view('error.404', array(), 404, 'page not found');
}

?>